# ITCross Starter Kit - Drupal 8 profile/distribution

ITCross Starter Kit is designed as a base for content websites, it saves several dozens of hours of development.

Demo website: http://starter-kit.itcross.org/

## Features

- [Docksal](https://github.com/docksal/docksal) powered
- [Composer based](https://gitlab.com/itcross/starter-kit/blob/master/composer.json), using [Composer template for Drupal projects](https://github.com/drupal-composer/drupal-project)
- [Our compilation of best-needed modules for content websites](https://gitlab.com/itcross/starter-kit/blob/master/web/profiles/itcross_starter_kit/itcross_starter_kit.info.yml#L60) and their configuration ([profile level](https://gitlab.com/itcross/starter-kit/tree/master/web/profiles/itcross_starter_kit/config),[project level](https://gitlab.com/itcross/starter-kit/tree/master/config))
- [Optional extensibility by Drupal Features](#drupal-features-optional-extensibility)
- [Barrio](https://www.drupal.org/project/bootstrap_barrio) based [Bootstrap 4 base theme](https://gitlab.com/itcross/starter-kit/tree/master/web/themes/custom/bootstrap_barrio_itcross)
- [The script to make a subtheme](https://gitlab.com/itcross/starter-kit/blob/staging/scripts/clone-base-theme.sh)
- Extended devel configuration is enabled and [separated](https://www.drupal.org/project/config_split) from production
- Devops scripts ([Docksal level](https://gitlab.com/itcross/starter-kit/tree/master/.docksal/commands), [project level](https://gitlab.com/itcross/starter-kit/tree/master/scripts))
- [Default content](https://gitlab.com/itcross/starter-kit/tree/master/web/profiles/itcross_starter_kit/content)
- Predefined Roles, Permissions, Users, [Shortcuts](itcross/starter-kit/blob/14-improve-the-project-description/web/profiles/itcross_starter_kit/config/shortcuts/shortcuts.yml) for Administration
- Established and configured `gulp` - [gulp-min](https://gitlab.com/itcross/starter-kit/tree/master/gulp-min)
- [GitLab CI/CD configuration](https://gitlab.com/itcross/starter-kit/blob/master/.gitlab-ci.yml) with building and testing jobs

## Quick start
  Docksal should be installed - https://docs.docksal.io/getting-started/setup/.
  ```
  git clone https://gitlab.com/itcross/starter-kit
  fin init profile
  ```
  On "Installing from the profile" step, the command-line output will display "Drupal User 1" (admin) username and password.
  Open http://itcross-starter-kit.docksal in your browser.

##  Profile / Distribution Development

### How to update

`fin composer update --with-dependencies`

### Add modules and themes

1. [Download contributed modules and themes using Composer](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#download-contributed-modules-and-themes-using-composer). Use `fin composer ...`.
2. Add a module/theme into `web/profiles/itcross_starter_kit/itcross_starter_kit.info.yml`.
3. Install a module/theme to have configuration changes.
3. `fin p-cex`, to save configuration, see below.

### Configuration Changes

The profile configuration files is stored in `web/profiles/itcross_starter_kit/config/install` directory.

1. `fin p-cex`. Make sure that local git repo is not changed - `git status`, your configuration has not to contain changes which are not exported.
2. Make any config changes (through the web interface), test them.
3. `fin p-cex`. Check changed files - `git status`.
4. Commit changes if all is ok.

[fin p-cex](https://gitlab.com/itcross/starter-kit/blob/master/.docksal/commands/p-cex) is a customised version of [drupal config:export](https://hechoendrupal.gitbooks.io/drupal-console/en/commands/config-export.html) for the profile.

### Default Content
[fin p-dcer](https://gitlab.com/itcross/starter-kit/blob/master/.docksal/commands/p-cex) is a customised version of `drush dcer` ("Default Content" module command) for the profile.
See `web/modules/contrib/default_content/README.md` for original `drush dcer` command.

Syntax: `fin p-dcer <entity_type> <entity_id>`
Examples:
- `fin p-dcer node` export all nodes.
- `fin p-dcer node 1` export node with ID=1.
- `fin p-dcer menu_link_content` export all menu links.

## Drupal Features: optional extensibility
- [itcross_itc_advanced_content](https://gitlab.com/itcross/starter-kit/-/tree/master/web/modules/custom/features/itcross_itc_advanced_content) Content type for News, Articles and Blog. Categories pages. Node page, views, blocks, templates.
- [itcross_itc_banner](https://gitlab.com/itcross/starter-kit/-/tree/master/web/modules/custom/features/itcross_itc_banner) Content type for Slick banners. View, block, template.
- [itcross_itc_product](https://gitlab.com/itcross/starter-kit/-/tree/master/web/modules/custom/features/itcross_itc_product) Content type for products. Categories pages. Node page, Views, blocks, templates.
- [itcross_itc_commerce](https://gitlab.com/itcross/starter-kit/-/tree/master/web/modules/custom/features/itcross_itc_commerce) Pre-configured Drupal Commerce with demo products and Dashboard.
- [itcross_itc_service](https://gitlab.com/itcross/starter-kit/-/tree/master/web/modules/custom/features/itcross_itc_service) Content type for services. Node page, view, block, templates.
- [itcross_itc_portfolio](https://gitlab.com/itcross/starter-kit/-/tree/master/web/modules/custom/features/itcross_itc_portfolio) Content type for portfolio items. Categories. Node page, views, blocks, templates.
- [itcross_itc_people](https://gitlab.com/itcross/starter-kit/-/tree/master/web/modules/custom/features/itcross_itc_people) Content type for people. View, block, templates.
- [itcross_itc_dashboard](https://gitlab.com/itcross/starter-kit/-/tree/master/web/modules/custom/features/itcross_itc_dashboard) Total Control Admin Dashboard for the project.
- [itcross_itc_contact](https://gitlab.com/itcross/starter-kit/-/tree/master/web/modules/custom/features/itcross_itc_contact) Contact page with contact form and Google map.

If you work with the profile, after a feature enabling the related configuration changes should be added into the feature, but not into the profile.

## Project

### Creating a project

1. Create a *new* repo with this repo content.
2. `fin init profile -nc` Create a project with no devel settings, this will be `prod` environment.
3. `fin profile2project` Follow the instructions of the command. This action updates [README.md](https://gitlab.com/itcross/starter-kit/blob/master/README.md).

## CI/CD examples

https://gitlab.com/itcross/starter-kit/-/wikis/CI/CD-Examples
