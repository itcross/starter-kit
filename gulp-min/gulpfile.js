var fs = require('fs');

// Read env_settings from file if exists
if (fs.existsSync('settings.json')) {
  settings = JSON.parse(fs.readFileSync('settings.json'));
} else {
  console.log("Error: the file settings.json not found! Run the command 'fin gulp' to generate this file.");
  process.exit(1);
}

//Gulp plugin
let gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
        pattern: '*'
    }),
    runTimestamp = Math.round(Date.now()/1000);

// Paths
let theme_path = settings.theme_path;
let path = {
  dst: {
    css: theme_path + 'css/_compiled',
    js: theme_path + 'js/_compiled',
    img: theme_path + 'img/_compiled',
    img_res: theme_path + 'img/_compiled/img_res',
    ifont: theme_path + 'fonts/_compiled',
  },
  src: {
    sass: theme_path + '_src/scss/**/*.{sass,scss}',
    js: theme_path + '_src/js/**/*.js',
    img: theme_path + '_src/img/**/*.{jpg,jpeg,gif,png,svg}',
    img_res: theme_path + '_src/img/img_res/*.{jpg,jpeg,png}',
    php: theme_path + '**/*.php',
    ifont: theme_path + '_src/icon-fonts/*.svg',
    icons: theme_path + '_src/icons/*.png',
  }
};

gulp.task('browser-sync', (done) => {
    $.browserSync.init({
        port : 4001,
        proxy: "itcross-starter-kit.docksal",
        open: false
    });
  done();
});

gulp.task("SCSS", (done) => {
  console.log("Generate CSS files from SCSS " + (new Date()).toString());
  gulp.src(path.src.sass)
    .pipe($.sourcemaps.init({
      loadMaps: true
    }))
    .pipe($.sass({ style: 'expanded' }))
    .on( 'error', $.notify.onError({
      title: 'SCSS Compilation Failed',
      message: '<%= error.message %>'
    }))
    .pipe($.autoprefixer("last 3 version","safari 5", "ie 8", "ie 9"))
    .pipe($.concat('style.css'))
    .pipe($.rename({suffix: '.min'}))
    .pipe($.cleanCss())
    .pipe($.filesize())
    .pipe($.sourcemaps.write(''))
    .pipe($.gulp.dest(path.dst.css))
    .pipe($.browserSync.stream());
  done();
});

gulp.task('JS', (done) => {
  console.log("Generate JS files from JS " + (new Date()).toString());
  gulp.src(path.src.js)
    .pipe($.babel({
      presets: [require('babel-preset-env')],
    }))
    .on( 'error', $.notify.onError({
      title: 'JS Compilation Failed',
      message: '<%= error.message %>'
    }))
    .pipe($.concat('app.js'))
    .pipe($.rename({suffix: '.min'}))
    .pipe($.jsmin())
    .pipe($.filesize())
    .pipe($.gulp.dest(path.dst.js))
    .pipe($.browserSync.stream());
  done();
});


gulp.task('IMG', (done) => {
  console.log("Generate Images files " + (new Date()).toString());
  gulp.src(path.src.img)
    .pipe($.imagemin([
      $.imagemin.gifsicle({interlaced: true}),
      $.imagemin.mozjpeg({progressive: true}),
      $.imagemin.optipng({optimizationLevel: 9}),
      $.imagemin.svgo({
        plugins: [
          {removeViewBox: true},
          {cleanupIDs: false}
        ]
      })
    ]))
    .on( 'error', $.notify.onError({
      title: 'IMG Compilation Failed',
      message: '<%= error.message %>'
    }))
    .pipe($.changed(path.dst.img))
    .pipe($.gulp.dest(path.dst.img))
    .pipe($.filesize())
    .pipe($.browserSync.stream());
  done();
});

gulp.task('IMG-Responsiv', (done) => {
  console.log("Generate responsive Images files " + (new Date()).toString());
  gulp.src(path.src.img_res)
    .pipe($.responsive({
      '**/*': [ {
        width: 320,
        rename: {
          prefix: 'small/',
        },
      }, {
        width: 768,
        rename: {
          prefix: 'medium/'
        },
        withoutEnlargement: true
      }, {
        width: 1200,
        rename: {
          prefix: 'large/'
        },
        withoutEnlargement: true
      } ]
    }, {
      quality: 80,
      progressive: true,
      compressionLevel: 9,
      withMetadata: false,
      errorOnEnlargement: false,
      errorOnUnusedConfig: false,
      errorOnUnusedImage: false
    }
    ))
    .on( 'error', $.notify.onError({
      title: 'IMG-Responsiv Compilation Failed',
      message: '<%= error.message %>'
    }))
    .pipe($.changed(path.dst.img_res))
    .pipe($.gulp.dest(path.dst.img_res));
  done();
});

gulp.task('PHP', (done) => {
  console.log("Change PHP " + (new Date()).toString());
  gulp.src(path.src.php)
    .pipe($.browserSync.stream());
  done();
});

gulp.task('Iconfont', (done) => {
  console.log("Generate Iconfont " + (new Date()).toString());
  gulp.src(path.src.ifont)
    .pipe($.iconfont({
      fontName: 'Itcross-icons',
      prependUnicode: true, 
      formats: ['ttf', 'eot', 'woff'],
      timestamp: runTimestamp,
    }))
    .on( 'error', $.notify.onError({
      title: 'Iconfont Compilation Failed',
      message: '<%= error.message %>'
    }))
    .pipe($.gulp.dest(path.dst.ifont))
    .pipe($.browserSync.stream());
  done();
});

gulp.task('Sprite', (done) => {
  console.log("Generate Sprite " + (new Date()).toString());
  let spriteData =
    gulp.src(path.src.icons) 
      .pipe($.spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css',
      }));
  spriteData.img
    .pipe(gulp.dest(path.dst.img));
  spriteData.css
    .pipe(gulp.dest(path.dst.css));
  done();
});

gulp.task("_watch", (done) => {
  $.watch(path.src.sass, gulp.series('SCSS'), function(vinyl) {
    gutil.log(gutil.colors.magenta(vinyl.event, vinyl.basename));
  });
  $.watch(path.src.js, gulp.series('JS'), function(vinyl) {
    gutil.log(gutil.colors.magenta(vinyl.event, vinyl.basename));
  });
  $.watch(path.src.php, gulp.series('PHP'), function(vinyl) {
    gutil.log(gutil.colors.magenta(vinyl.event, vinyl.basename));
  });
  $.watch(path.src.img, gulp.series('IMG'), function(vinyl) {
    gutil.log(gutil.colors.magenta(vinyl.event, vinyl.basename));
  });
  $.watch(path.src.img_res, gulp.series('IMG-Responsiv'), function(vinyl) {
    gutil.log(gutil.colors.magenta(vinyl.event, vinyl.basename));
  });
  $.watch(path.src.ifont, gulp.series('Iconfont'), function(vinyl) {
    gutil.log(gutil.colors.magenta(vinyl.event, vinyl.basename));
  });
  $.watch(path.src.icons, gulp.series('Sprite'), function(vinyl) {
    gutil.log(gutil.colors.magenta(vinyl.event, vinyl.basename));
  });
  done();
});

const compile = gulp.parallel(['_watch', 'browser-sync', 'SCSS', 'JS', 'PHP', 'IMG', 'IMG-Responsiv', 'Iconfont', 'Sprite']);
gulp.task('default', compile);
