css-generator-sass-gulp
=======================

Project that generates the CSS from Sass (.scss). The build tool used is Gulp. 

### Install and run the project
#### 1. Install [Node.js](http://nodejs.org/) v.8.x on your machine
#### 2. Install gulp globally
`npm install -g gulp`
#### 3. Install gulp in your project devDependencies
`npm install --save-dev gulp`
#### 4. Install the required gulp plugins
`npm install --save-dev gulp-sass gulp-minify-css gulp-rename gulp-autoprefixer gulp-babel babel-preset-react babel-plugin-transform-react-jsx babel-preset-es2016 gulp-watch gulp-util browser-sync`
#### 5. Run gulp tasks in your project's root to generate css or watch the files for modication (it automatically generates css by modification)
* `gulp sass` or just `gulp`
* `gulp watch`

## Associated posts on [Codingpedia.org](http://www.codingpedia.org)
* [CSS Preprocessors – Introducing Sass to Podcastpedia.org](http://www.codingpedia.org/ama/css-preprocessors-introducing-sass-to-podcastpedia-org/) 
* [How to use Gulp to generate CSS from Sass/scss](http://www.codingpedia.org/ama/how-to-use-gulp-to-generate-css-from-sass-scss/)
* [Simple trick to create a one line horizontal responsive menu with CSS only – Podcastpedia.org](http://www.codingpedia.org/ama/simple-trick-to-create-a-one-line-horizontal-responsive-menu-with-css-only-podcastpedia-org/)


How to use
=======================

#### 1. Go to webroot directory.

#### 2. Run gulp.

* `fin gulp`

```

 [Browsersync] Access URLs:
 -----------------------------------
       Local: http://localhost:4000
    External: http://172.18.0.7:4000
 -----------------------------------
          UI: http://localhost:3001
 UI External: http://172.18.0.7:3001
 -----------------------------------

```
#### 3. Copy the External Url (http://172.18.0.7:4000) from the console and open this Url to the browser.


### UI

#### 1. Browsersync includes a user-interface that is accessed via a separate port (http://172.18.0.7:3001). The UI allows to controls all devices, push sync updates and much more.


### Notice

1. Default port (3000) has been changed to port (4000).
2. External Url is available on all devices that are connected to the Wi-fi router.
