#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR; cd ../..

PROJECT_ROOT=${PWD}

cd ${PROJECT_ROOT}/gulp-min/ && npm i --silent && npm i fsevents -f && gulp --gulpfile gulpfile-ci.js
cd ${PROJECT_ROOT}/web/themes/custom/bootstrap_barrio_itcross && npm install
