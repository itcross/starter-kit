#!/usr/bin/env bash

#-------------------------- Helper functions --------------------------------

confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY])
            true
            ;;
        *)
            false
            ;;
    esac
}

# Move a file.
# Skips if the destination file already exists.
# @param $1 source file
# @param $2 destination file
move_file()
{
	local source="$1"
	local dest="$2"

	if [[ ! -f $dest ]]; then
    echo "Moving $filename from '${source}' to '${dest}'..."
		mv $source $dest
	else
		echo "${dest} already in place."
	fi
}
